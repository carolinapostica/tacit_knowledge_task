var express = require('express');
var app = express();


app.use(express.static(__dirname + '/../web'));

app.all('*', function (req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Content-Type");

return next();
});

var server = app.listen('9000', function () {
    console.log('Server port: ' + server.address().port);
});
