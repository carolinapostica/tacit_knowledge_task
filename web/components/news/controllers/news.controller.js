(function() {
    'use strict';
    angular
        .module('theJournal')
        .controller('newsController', ['FeedService', '$http', '$state', '$sce', '$window', newsController]);

    function newsController(FeedService, $http, $state, $sce, $window) {
        var self = this;
        var feedSrc = [{
                title: 'Diez',
                url: 'http://www.diez.md/feed'
            },
            {
                title: 'Unimedia',
                url: 'http://www.unimedia.info/rss/news.xml'
            },
            {
                title: 'Agora',
                url: 'http://www.agora.md/rss/news'
            },
            {
                title: 'Zugo',
                url: 'http://www.zugo.md/rssfeed.xml'
            }
        ];
        this.feeds = [];
        self.actuals = [];
        var num = 100;
        var currentDate = new Date();
        self.yesterdayDate = currentDate.getTime() - 86400000;
        self.maxSize = 3;
        self.itemsPerRow = $window.outerWidth > 480 ? 4 : 2;
        self.bigCurrentPage = 1;
        self.itemsPerPage = $window.outerWidth > 480 ? 12 : 6;

        this.loadFeed = function loadFeed(feedSrc) {
            // get all news
            var url = 'https://api.rss2json.com/v1/api.json?rss_url=' + encodeURIComponent(feedSrc) + '&api_key=xhzoily7ojc5pawhsnmnuvuwxb8entuetl8s2jph&order_by=pubDate&order_dir=desc' + '&count=' + num;
            FeedService.parseFeed($sce.trustAsResourceUrl(url)).then(function(res) {
              console.log(res.data);
                var result = res.data.items;
                for (var j = 0; j < result.length; j++) {
                    self.feeds.push(result[j]);
                }
                // Transform iso data to miliseconds
                for (var k = 0; k < self.feeds.length; k++) {
                    var date = new Date(self.feeds[k].publishedDate);
                    self.feeds[k].publishedDate = date.getTime();

                }
            });

          // get news for carousel
          var urlSrc = 'http://agora.md/rss/actual.xml';
          var number = 8;

            var urlActual = 'https://api.rss2json.com/v1/api.json?rss_url=' + encodeURIComponent(urlSrc) + '&api_key=xhzoily7ojc5pawhsnmnuvuwxb8entuetl8s2jph&order_by=pubDate&order_dir=desc' + '&count=' + number;
            FeedService.parseFeed($sce.trustAsResourceUrl(urlActual)).then(function(res) {
                self.actuals = res.data.items;
                console.log(res.data.items);
                // responsiveness of the carousel
                var appWindow = angular.element($window);
                var chunkSize = $window.outerWidth > 480 ? 2 : 1;
                self.chunkedSlides = chunk(self.actuals, chunkSize);

                function chunk(arr, size) {
                    var newArr = [];
                    var arrayLength = arr.length;
                    for (var l = 0; l < arrayLength; l += size) {
                        newArr.push(arr.slice(l, l + size));
                    }
                    return newArr;
                }
            });

        }
        // Load all news
        for (var i = 0; i < feedSrc.length; i++) {
            self.loadFeed(feedSrc[i].url);
        }

    };
})();
