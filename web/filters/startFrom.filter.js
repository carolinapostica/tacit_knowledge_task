(function () {
angular
.module('theJournal')
.filter('startFrom', function() {
        return function(input, start, end) {
            start = +start; //parse to int
            return input.slice(start, end);
        }
    });

}());
