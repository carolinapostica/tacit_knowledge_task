(function() {
    'use strict';
    angular
        .module('theJournal')
        .filter('dayFilter', function() {

            return function(input, period) {
                var filterFunction = function(item) {
                    return item.publishedDate <= period;
                };
                return input.filter(filterFunction);
            };
        });

}());
