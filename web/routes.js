(function() {
    'use strict';
    angular
        .module('theJournal')
        .config(routes);

    function routes($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state({
                name: 'news',
                url: '/',
                controller: 'newsController',
                controllerAs: 'news',
                templateUrl: 'components/news/templates/news.template.html'
            })
          
        $urlRouterProvider.otherwise('/');
    };
}());
