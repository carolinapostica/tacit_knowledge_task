(function() {
    'use strict';

    angular
        .module('theJournal')
        .factory('FeedService', ['$http', FeedService]);

    function FeedService($http) {
        return {

            parseFeed: function(url) {
                var feeds = [];
                feeds = $http.jsonp(url);
                return feeds;
            }
        }
    }
}());
